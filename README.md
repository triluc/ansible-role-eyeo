# Eyeo Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [eyeo](https://www.eyeo.com/)-specific resources.
